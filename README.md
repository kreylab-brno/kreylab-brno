# kreylab-brno

Krey's experiments, projects and issues about FabLab Brno.

### Community

This project is using multiple platforms that bridge their messages so that message send on platform A will be shown on platform B as well.

* **Matrix:** [#kreylab-space:matrix.org](https://matrix.to/#/#kreylab-space:matrix.org)
* **IRC:** <a href="https://webirc.hackint.org/#ircs://irc.hackint.org/kreylab-general-cs">#kreylab-* on Hackint.org</a> \<3
* **Shitcord:** [KW7Cekr7zB](https://discord.gg/KW7Cekr7zB)